const md = 991
const sm = 767

$(document).ready(function () {
  $('.popup-get-consult-button').on('click', function (event) {
    $('.popup-get-consult').fadeIn();
  });
  $('.popup__close').on('click', function (event) {
    $('.popup-get-consult').fadeOut();
  });

  $('.js__slider-1').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button class="arrow arrow--prev-circle"></button>',
    nextArrow: '<button class="arrow arrow--next-circle"></button>'
  });

  $('.js__slider-4').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: md,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 2,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      },
      {
        breakpoint: sm,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      }
    ]
  });
  $('.js__slider-3').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<button class="arrow arrow--prev"></button>',
    nextArrow: '<button class="arrow arrow--next"></button>',
    responsive: [
      {
        breakpoint: md,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 2,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      },
      {
        breakpoint: sm,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      }
    ]
  });
});


